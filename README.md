![Image](Screen_Shot_2.png)

## Video:

[![Automate Playing Adhan on Google Home](http://img.youtube.com/vi/E_qlDUX1QNU/0.jpg)](http://www.youtube.com/watch?v=E_qlDUX1QNU "Automate Playing Adhan on Google Home")

### Download:

[Project files](https://gitlab.com/shariatpurbasi/islamic-prayer-call-assistant/-/archive/master/islamic-prayer-call-assistant-master.zip)

### Modify: **automations.yaml file**

> For 'entity*id' change the device name to your device name.  
> \_example:* **entity_id: media_player.living_room_speaker**  
>  **entity_id: media_player.[name]**

> Update **_media_content_id:_** with the local IP of the host where you are running the docker container. Validate port 1001 is accessible.\
> media_content_id: http://192.168.1.159:1001/azan2.mp3 \
> media_content_id: http://192.168.1.159:1001/Fajar-Adan.mp3

### Modify: **configuration.yaml**

> Follow the instruction at the bottom of the configuration.yaml file

---

### Commands:

Open Terminal and navigate to the islamic-prayer-call-assistant-master download folder.

Run the following commands:

```
docker-compose down
docker system prune -a
docker-compose up -d
```

\
Open the browser and visit the following URL to\
manage the Home Assistant app You have to create\
an account to get started

http://<Local_IP>:8123/ \
or \
http://localhost:8123/
\
\
Navigate to http://localhost:1001, or http://<Local_IP>:1001\
Validate the API data and audio files.
Note the Home Assistant app is configured to refresh data every 30 minutes. The API container pulls new prayer schedule every day, once at 12:10 am.
