#!/bin/bash

D=$(date "+%Y-%m-%d %I.%M.%S %p %Z")
adhan_D=$(date "+%d-%m-%Y")

# Function to fetch public IP with multiple fallbacks
get_public_ip() {
  # Primary service
  myIP=$(curl -L -s --max-time 10 https://ifconfig.me)
  
  # Backup services
  if [ -z "$myIP" ]; then
    myIP=$(curl -L -s --max-time 10 https://api.ipify.org)
  fi
  
  if [ -z "$myIP" ]; then
    myIP=$(curl -L -s --max-time 10 https://ipinfo.io/ip)
  fi
  
  if [ -z "$myIP" ]; then
    myIP=$(curl -L -s --max-time 10 http://icanhazip.com)
  fi
  
  # Final check
  if [ -z "$myIP" ]; then
    echo "$D - Error: Unable to fetch public IP from any service" > "$log_path"
    exit 1
  fi
}

primary_path="/code/ajan_api.json"
log_path="/code/job-log.txt"

if [ ! -d "/code" ]; then
  primary_path="ajan_api.json"
  log_path="job-log.txt"
fi

get_public_ip

lat=$(curl -L -s --max-time 10 http://ip-api.com/csv/?fields=lat)
lon=$(curl -L -s --max-time 10 http://ip-api.com/csv/?fields=lon)

if [ -z "$lat" ] || [ -z "$lon" ]; then
  echo "$D - Error: Unable to fetch location data" > "$log_path"
  exit 1
fi


curl -s "http://api.aladhan.com/v1/timings/$adhan_D?latitude=$lat&longitude=$lon&method=2&school=2" -o "$primary_path"
if [ $? -eq 0 ]; then
  echo "$D - Successfully fetched prayer times" > "$log_path"
else
  echo "$D - Error: Failed to fetch prayer times" > "$log_path"
  exit 1
fi
